package tombola;

import java.util.ArrayList;
import java.util.Arrays;

public class Cartella {
	
	public int[] row1;
	public int[] row2;
	public int[] row3;
	public ArrayList<Integer> extractedNumbers = new ArrayList<Integer>();
	
	public Cartella(int[] row1, int[] row2, int[] row3) {
		this.row1 = row1;
		this.row2 = row2;
		this.row3 = row3;
	}
	
	@Override
	public String toString() {
		return "Riga 1: " + Arrays.toString(row1) + ", Riga 2: " + Arrays.toString(row2) + ", Riga 3: " + Arrays.toString(row3);
	}
	
}
