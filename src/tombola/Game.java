package tombola;

import java.lang.Math;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Game {
	
	public static int repetitions = 0;
	public static int[] generatedNumbers = new int[15];

	public static void main(String[] args) {
		Cartella cartella1 = generateCartella();
		Cartella cartella2 = generateCartella();
		Cartella cartella3 = generateCartella();
		Cartella[] cartelle = {cartella1, cartella2, cartella3};
		System.out.println("Cartella 1: " + cartella1.toString() +
				"\nCartella 2: " + cartella2.toString() +
				"\nCartella 3: " + cartella3.toString());
		Tombola(cartelle);
	}
	
	private static Cartella generateCartella() {
		repetitions = 0;
		generatedNumbers = new int[15];
		return new Cartella(generateRow(), generateRow(), generateRow());
	}
	
	private static int[] generateRow() {
		int[] row = new int[5];
		for (int i = 0; i <= 4; i++) {
			int rand = generateRandomNumber();
			generatedNumbers[repetitions] = rand;
			row[i] = rand;
			repetitions++;
		}
		return row;
	}
	
	private static int generateRandomNumber() {
		int range = (90 - 1) + 1;
		int random = (int)(Math.random() * range);
		boolean repeated = IntStream.of(generatedNumbers).anyMatch(x -> x == random);
		return repeated ? generateRandomNumber() : random;
	}
	
	private static void checkRows(Cartella c, int index, int number) {
		if (IntStream.of(c.row1).anyMatch(x -> x == number) || 
				IntStream.of(c.row2).anyMatch(x -> x == number) || 
				IntStream.of(c.row3).anyMatch(x -> x == number)) 
		{
			if (c.extractedNumbers.contains(number)) {
				System.out.println("Numero " + number + " contenuto nella cartella " + (index + 1) + " ma già estratto!");
			} else {
				System.out.println("Numero " + number + " contenuto nella cartella " + (index + 1));
				c.extractedNumbers.add(number);
			}
		}
	}
	
	private static void Tombola(Cartella[] cartelle) {
		Scanner input = new Scanner(System.in);
		do {
			System.out.print("Numero estratto: ");
			int number = Integer.parseInt(input.nextLine());
			int index = 0;
			for (Cartella c : cartelle) {
				checkRows(c, index, number);
				index++;
			}
			System.out.println("Numeri estratti della cartella 1: " + cartelle[0].extractedNumbers + " della cartella 2: " + cartelle[1].extractedNumbers + " della cartella 3: " + cartelle[2].extractedNumbers);
		} while (cartelle[0].extractedNumbers.size() < 5 && cartelle[1].extractedNumbers.size() < 5 && cartelle[2].extractedNumbers.size() < 5);
		System.out.println("Tombola!!!");
		input.close();
	}

}
